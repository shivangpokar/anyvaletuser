import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapScreenPage } from './map-screen.page';

describe('MapScreenPage', () => {
  let component: MapScreenPage;
  let fixture: ComponentFixture<MapScreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapScreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapScreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
